#include "game.h"

int count_draw = 0;

void init_deck() {
	char ranks[13] = {'1','2','3','4','5','6','7','8','9','X','V','L','K'};
	char suits[4] = {'S','H','C','D'};
	
	int counter = 0; 
	for(int i=0; i<4; i++) { 
		for(int j=0; j<13; j++) {
			if(j>=10) {
				struct Card card = {ranks[j], suits[i], 10};
				deck[counter] = card;
			} else {
				int value = j+1;
				struct Card card = {ranks[j], suits[i], (uint16_t) value}; 
				deck[counter] = card;
			}
			counter++;
		}
	}
}

void shuffle() {

	srand((unsigned int) time(NULL));
	int size_deck = 52;
	int i, size;
	size = size_deck;
	for(i=0; i < size_deck - 1; i++) {
		struct Card tmp;
		int index = rand()%size;
		tmp = deck[index];
		deck[index] = deck[size-1];
		deck[size_deck-i-1] = tmp;
		size--;
	}
}


bool send_card_to_player(int client_fd) {
	struct Card card = {deck[count_draw].rank,deck[count_draw].suit, htons(deck[count_draw].value)};
	
	return write(client_fd, &card, sizeof(card)) < 0;
}

bool send_info_to_player(int client_fd, struct Game game) {
	return write(client_fd, &game, sizeof(game)) < 0;
}

bool send_dealer_info_to_player(int client_fd, struct Dealer dealer) {
	return write(client_fd, &dealer, sizeof(dealer)) < 0;
}

bool send_message(int client_fd, struct Message m) {
	
	return write(client_fd, &m, sizeof(m)) < 0;

}
