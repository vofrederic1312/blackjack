# Blackjack

Projet réalisé pour le cours de Programmation Réseaux de Cervelle Julien à l'UPEC

Règles : 

Le jeu se lance avec uniquement 2 joueurs. Dans le cas d'une entrée supplémentaire d'un client, la connexion sera refusée.

- [ ] Le croupier piochera jusqu'à atteindre 16 ou plus.
- [ ] Dans le cas où le croupier dépasse 21, les joueurs seront considérés comme gagnants peut importe l'issue.
- [ ] Dans le cas où un joueur dépasse 21 et que le croupier ne dépasse pas 21, le joueur aura perdu.
- [ ] A la fin, le score d'un joueur inférieur ou égal à celui du croupier sera considéré comme une défaite.
- [ ] A la fin, le score d'un joueur supérieur à celui du croupier sera considéré comme une victoire.

## Comment compiler et lancer le projet sous Linux

Rentrez tout d'abord dans le dossier contenant le projet.

```
$ cd "folder/that/contains/the/project"

$ make
```
Ouvrez 3 terminales pour le tester :

Premièrement, lancez le server;
```
$ (Terminal 1) ./server
```
Puis les clients.
```
$ (Terminal 2) ./client
$ (Terminal 3) ./client
```
## Comment tester ou jouer au jeu

Lorsque les deux clients seront connectés, ils recevront une carte chacun. Le croupier piochera aussi une carte.

Le joueur pourra jouer quand ce sera son tour. Lorsqu'il aura utilisé une commande, le prochain joueur pourra le faire à son tour.

- Commandes à utiliser ensuite :

- [ ] "Tirer" pour piocher une carte;
- [ ] "Rester" pour rester et attendre la fin de la partie pour connaître le résultat.
- [ ] "Info" pour connaître les cartes du croupier ainsi que les autres joueurs.

A chaque fin de tour, un résumé des informations de la partie sera affiché.

Le serveur et les clients se fermeront lorsque toutes les issues seront connues.
