# Blackjack

Bugs connus :

- [ ] Fermeture soudaine du serveur quand un client quitte avant la fin de la partie ou même pendant l'attente de nouveaux joueurs.
- [ ] On peut écrire des commandes sans retour du serveur quand le client a terminé de jouer (S'il reste, ou s'il a perdu en dépassant 21 par exemple).
- [ ] Si le serveur crash, les clients continuent de fonctionner et peuvent écouter infiniment.

Evolutions possibles (Pour chacune en moins d'une semaine) :

- [ ] Traiter les déconnexions soudaines des clients.
- [ ] Ajouter plus de joueurs au projet.
- [ ] Améliorer l'algorithme de traitement des cartes jouées (principalement de le raccourcir et faire du refactoring).
- [ ] Faire en sorte de relancer une partie sans que le serveur s'arrête.
- [ ] Créer un système de points en plus de la victoire/défaite.
