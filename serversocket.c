#include "serversocket.h"

bool init_socket() {
	return (server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0;
}

bool bind_socket() {
        return bind(server_socket, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0;
}

bool listen_socket() {
	return (listen(server_socket, MAXPLAYERS)) < 0;
}

bool accept_client() {
	return (connect_fd = accept(server_socket, (struct sockaddr *)&servaddr, (socklen_t*) &length)) < 0;
}

void init_addr() {
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);
}

/* Allow clients to connect. */
void select_accept_client() {

	length = sizeof(cliaddr);
	
	if(accept_client()) { 
		perror("Not accepting...\n");
		exit(EXIT_FAILURE);
	} 
	

	game.players[current_client].client_fd = (uint16_t) connect_fd; // Store the fd's client in an array of clients
	FD_SET(game.players[current_client].client_fd, &fds); 
				    			
	if (game.players[current_client].client_fd > fdmax) {
		fdmax = game.players[current_client].client_fd;
	}
	
	strcpy(m.message, "Connexion\n"); 
	m.type = CONTINUE;

	if(send_message(connect_fd, m)) { // Send message to client to notify him that he is connected.
		perror("Write error");
		exit(EXIT_FAILURE);
	}
	
	current_client++; // Increment the number of clients connected. 
		
	if(current_client == MAXPLAYERS) { // The game start if the number of clients reaches the maximum allowed by the server.		
		start_game();
	}
}

/* Refuse the connecting client when the maximum of players is reached. */
void refuse_client() {
	length = sizeof(cliaddr);
	
	if(accept_client()) { 
		perror("Not accepting...\n");
		exit(EXIT_FAILURE);
	} 
	
	strcpy(m.message, "Deconnexion... Serveur complet\n");
	m.type = NOT_VALID;
	
	if(send_message(connect_fd, m)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
}

void dealer_draw() {
	if(!dealer.stop) { // if dealer didn't decided to stop drawing.
		struct Card card = {deck[count_draw].rank,deck[count_draw].suit,deck[count_draw].value};
		dealer.cards[dealer.number_cards] = card;
		dealer.total_value_cards = (uint16_t)(dealer.total_value_cards + card.value); 
		dealer.number_cards++;
		count_draw++;
	} 
	
	if(dealer.total_value_cards >= 16) { // if dealer reach value of 16 or more, he stops.
		dealer.stop = true;
	}
}

void start_game() {
	int count = 0;
	
	dealer_draw(); // Start with the dealer's draw then distribute cards for players
				
	for(int j = 0; j <= fdmax; j++) {			 
		if(FD_ISSET(j, &fds)) {
			if(j != server_socket) {
				if(send_card_to_player(j)) { 
					perror("Write error");
					exit(EXIT_FAILURE);
				}
					
				struct Card card = {deck[count_draw].rank,deck[count_draw].suit,deck[count_draw].value};
					
				// Store cards and game's information in array of clients
				game.players[count].cards[game.players[count].number_cards] = card;
				game.players[count].total_value_cards = (uint16_t)(game.players[count].total_value_cards + card.value); 
				game.players[count].number_cards++;
				
				count_draw++;
				count++;
			}
		}
	}	
	
	current_player = game.players[rounds].client_fd; // The current player is the first to connect
}

void handle_command(int fd) {
	if(read(fd, &m, sizeof(m)) < 0) { // Read the command by the client.
		perror("Read error");
		exit(EXIT_FAILURE);
	}
	
	if(fd == current_player) { // if the client who writes is recognized as the current player.
		if(strcmp(m.message, "Tirer") == 0) {
			draw(fd);
		} else if (strcmp(m.message, "Rester") == 0) {
			stay(fd);
		} else if (strcmp(m.message, "Info") == 0) {
			info(fd);
		} else {
			handle_wrong_command(fd);		
		} 
	} else {

		handle_wrong_turn(fd);
	}
}

void draw(int fd) {
	strcpy(m.message, "Ta carte !\n");
	m.type = PLAY;
	
	if(send_message(fd, m)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
	
	if(send_card_to_player(fd)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
	
	struct Card card = {deck[count_draw].rank,deck[count_draw].suit,deck[count_draw].value};
		
	// Store cards and game's information in array of clients
	game.players[rounds].cards[game.players[rounds].number_cards] = card;
	game.players[rounds].total_value_cards = (uint16_t)(game.players[rounds].total_value_cards + card.value);
	game.players[rounds].number_cards++;
	
	// Handle the "draw" to know the result of his drawing.
	handle_draw(fd);
	
	count_draw++;	
	
	change_round();
		
	// The next client will be the current player.
	current_player = game.players[rounds].client_fd;
			
}

void stay(int fd) {
	game.players[rounds].status.stand = true;
	
	strcpy(m.message, "Tu as décidé de rester..\n");
	m.type = STAY;
	
	if(send_message(fd, m)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
	
	// Handle the "draw" to know the result of his drawing.
	handle_stay(fd);
	
	change_round();
	
	// The next client will be the current player.
	current_player = game.players[rounds].client_fd;
}

/* Give info for client like dealer's and player cards */
void info(int fd) {
	strcpy(m.message, "Voici les informations des autres joueurs !\n");
	m.type = INFO;
	
	if(send_message(fd, m)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}	
	
	if(send_dealer_info_to_player(fd, dealer)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
	
	if(send_info_to_player(fd, game)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
	
}

void handle_wrong_command(int fd) {
	strcpy(m.message, "Mauvaise commande : Tirer - Rester - Info\n");
	m.type = NOT_VALID;
	
	if(send_message(fd, m)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}	
}

/* To recognize if the client who's playing is recognized as the current player who's allowed to play */
void handle_wrong_turn(int fd) {
	strcpy(m.message, "Pas ton tour.. \n");
	m.type = NOT_VALID;

	if(send_message(fd, m)) {
		perror("Write error");
		exit(EXIT_FAILURE);
	}
}

void change_round() {

	int tmp_round = 0;
	int count_stop_players = 0;
	
	for(int i=0; i<MAXPLAYERS; i++) {
		if(game.players[i].status.stop||game.players[i].status.stand) count_stop_players++;
	}
	
	if(count_stop_players == 0) { // Case where no client is stopped
		do { // Loop until the end of the round.
			if(rounds == MAXPLAYERS - 1) {
				rounds = 0; 
				if (!dealer.stop) { // if the round is ended and dealer doesn't stop to play
					dealer_draw();
					handle_dealer_draw();
				}
					
				for(int i=0; i<MAXPLAYERS; i++) { // Send informations for players at the end of the round and to know if the game continue or not.
					int count_stop = 0;
					
					if(send_dealer_info_to_player(game.players[i].client_fd, dealer)) {
						perror("Write error");
						exit(EXIT_FAILURE);
					}
					
					if(send_info_to_player(game.players[i].client_fd, game)) {
						perror("Write error");
						exit(EXIT_FAILURE);
					}
					
					for(int i=0; i<MAXPLAYERS; i++) {
						if(game.players[i].status.stop||game.players[i].status.stand) count_stop++;
					}
						
					if(count_stop == MAXPLAYERS && dealer.stop) { // if all players and dealer stopped.
						strcpy(m.message, "Exit..\n");
						m.type = EXIT;
						
						if(send_message(game.players[i].client_fd, m)) {
							perror("Write error");
							exit(EXIT_FAILURE);
						}
						
						running = false; // Will close the server
					} else {
						strcpy(m.message, "Continue..\n");
						m.type = CONTINUE;
						
						if(send_message(game.players[i].client_fd, m)) {
							perror("Write error");
							exit(EXIT_FAILURE);
						}
					}
						
				}
			}
			else {
				++rounds;
			} 	
			
			tmp_round++;
			
		} while(((game.players[rounds].status.stop||game.players[rounds].status.stand) && tmp_round < MAXPLAYERS)); 
		
	} else if(count_stop_players == 1){ // Case when 1 client stopped
		int count_round = 0; // In the case, the dealer is stopped, so the player doesn't change round infinitely
		do { // Loop until all players are stopped.
			if(rounds == MAXPLAYERS - 1) {
				
				rounds = 0; 
				if (!dealer.stop) { // if the round is ended and dealer doesn't stop to play
					dealer_draw();
					handle_dealer_draw();
				}
					
				for(int i=0; i<MAXPLAYERS; i++) { // Send informations for players at the end of the round and to know if the game continue or not.
					int count_stop = 0;
					
					if(send_dealer_info_to_player(game.players[i].client_fd, dealer)) {
						perror("Write error");
						exit(EXIT_FAILURE);
					}
					
					if(send_info_to_player(game.players[i].client_fd, game)) {
						perror("Write error");
						exit(EXIT_FAILURE);
					}
					
					for(int i=0; i<MAXPLAYERS; i++) {
						if(game.players[i].status.stop||game.players[i].status.stand) count_stop++;
					}
						
					if(count_stop == MAXPLAYERS && dealer.stop) { // if all players and dealer stopped.
						strcpy(m.message, "Exit..\n");
						m.type = EXIT;
						
						if(send_message(game.players[i].client_fd, m)) {
							perror("Write error");
							exit(EXIT_FAILURE);
						}
						
						running = false; // Will close the server
					} else {
						strcpy(m.message, "Continue..\n");
						m.type = CONTINUE;
						
						if(send_message(game.players[i].client_fd, m)) {
							perror("Write error");
							exit(EXIT_FAILURE);
						}
					}
						
				}
				
				if(count_round > 1 && dealer.stop) {
					break;
				}
				
			} else {
				++rounds;
			} 
			count_round++;	
			
		} while((game.players[rounds].status.stop||game.players[rounds].status.stand)); 
		
	} else { // Case when all clients stopped
		if(!dealer.stop) {
			while (!dealer.stop) { // Loop if all players stopped but not the dealer
				if(rounds == MAXPLAYERS - 1) {
					rounds = 0; 
					if (!dealer.stop) {
						dealer_draw();
						handle_dealer_draw();
					}
						
					for(int i=0; i<MAXPLAYERS; i++) {
						
						if(send_dealer_info_to_player(game.players[i].client_fd, dealer)) {
							perror("Write error");
							exit(EXIT_FAILURE);
						}
						
						if(send_info_to_player(game.players[i].client_fd, game)) {
							perror("Write error");
							exit(EXIT_FAILURE);
						}
							
						if(dealer.stop) {
							strcpy(m.message, "Exit..\n");
							m.type = EXIT;
							
							if(send_message(game.players[i].client_fd, m)) {
								perror("Write error");
								exit(EXIT_FAILURE);
							}
							
							running = false;
						} else {
							strcpy(m.message, "Continue..\n");
							m.type = CONTINUE;
							
							if(send_message(game.players[i].client_fd, m)) {
								perror("Write error");
								exit(EXIT_FAILURE);
							}
						}
					}
				}
				else {
					++rounds;
				} 
				
			} 		
		} else {
			for(int i=0; i<MAXPLAYERS; i++) {
					
				if(send_dealer_info_to_player(game.players[i].client_fd, dealer)) {
					perror("Write error");
					exit(EXIT_FAILURE);
				}
				
				if(send_info_to_player(game.players[i].client_fd, game)) {
					perror("Write error");
					exit(EXIT_FAILURE);
				}
				
				strcpy(m.message, "Exit..\n");
					m.type = EXIT;
					
					if(send_message(game.players[i].client_fd, m)) {
						perror("Write error");
						exit(EXIT_FAILURE);
				}
						
				running = false;
			}	
		}
	}
}

/* handle_draw, handle_stay, handle_dealer_draw are actually the rules for the blackjack's project */

void handle_draw(int fd) {
	for(int i=0; i<MAXPLAYERS; i++) {
		if(fd == game.players[i].client_fd) {
			if(game.players[i].total_value_cards > 21) {
				game.players[i].status.win = false;
				game.players[i].status.stop = true;
			}

			else if(game.players[i].total_value_cards > dealer.total_value_cards && dealer.stop) {
				game.players[i].status.win = true;
				game.players[i].status.stop = true;
			} 
		}
	}
}

void handle_stay(int fd) {
	for(int i=0; i<MAXPLAYERS; i++) {
		if(fd == game.players[i].client_fd) {
			if(game.players[i].total_value_cards <= dealer.total_value_cards && dealer.total_value_cards < 21 && dealer.stop) {
				game.players[i].status.win = false;
				game.players[i].status.stop = true;
				game.players[i].status.stand = true;
			}

			else if(game.players[i].total_value_cards > dealer.total_value_cards && dealer.stop) {
				game.players[i].status.win = true;
				game.players[i].status.stop = true;
				game.players[i].status.stand = true;
				
			} 
		}
	}
}

void handle_dealer_draw() {
	for(int i=0; i<MAXPLAYERS; i++) {
		
		if(game.players[i].total_value_cards <= dealer.total_value_cards && game.players[i].status.stand && dealer.stop) {
			game.players[i].status.win = false;	
			game.players[i].status.stop = true;
		}

		else if(game.players[i].total_value_cards > dealer.total_value_cards && game.players[i].total_value_cards < 21 && dealer.stop) {
			game.players[i].status.win = true;
			game.players[i].status.stop = true;
		}
		
		if(dealer.total_value_cards > 21) {
			game.players[i].status.win = true;
			game.players[i].status.stop = true;
		} else if(dealer.total_value_cards == 21) {
			game.players[i].status.win = false;
			game.players[i].status.stop = true;
		}
	}
}

