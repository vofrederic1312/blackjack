#include "clientsocket.h"
   
int main() {
	running = true;
	waiting = true;
	total_value_player = 0;
	
	struct Card card;
	struct Message m;
	struct Game game;
	struct Dealer dealer;

	if(init_socket()) {
		perror("Socket creation failed..\n");
		exit(EXIT_FAILURE);
	};

	init_addr(&servaddr);
	
	if(connect_server()) {
		perror("Connection to server failed..\n");
		exit(EXIT_FAILURE);
	}
	
	if(read(clientSockFd, &m, sizeof(m)) < 0) {
		exit(EXIT_FAILURE);
	}
	
	if(m.type == CONTINUE) { // if the connection is allowed, beacause the server can be full.
	
		if(read(clientSockFd, &card, sizeof(card)) < 0) {
			exit(EXIT_FAILURE); 
		}
		
		printf("Carte :\n");
		printf("Rank -> %c - Suite -> %c - Valeur-> %d\n", card.rank, card.suit, ntohs(card.value));
		
		total_value_player += ntohs(card.value);
		
		while(running) {
			
			scanf("%[^\n]", m.message);
			m.type = 'n'; 
			fgetc(stdin);
			
			if(send_message(clientSockFd, m)) {
				exit(EXIT_FAILURE);
			}
			
			if(read(clientSockFd, &m, sizeof(m)) < 0) {
				exit(EXIT_FAILURE);
			}
			
			if(m.type == PLAY) {	
				
				if(read(clientSockFd, &card, sizeof(card)) < 0) {
					exit(EXIT_FAILURE); 
				}
				
				printf("Carte :\n");
				printf("Rank -> %c - Suite -> %c - Valeur-> %d\n", card.rank, card.suit, ntohs(card.value));
				
				if(read(clientSockFd, &dealer, sizeof(dealer)) < 0) {
					exit(EXIT_FAILURE);
				}
				
				if(read(clientSockFd, &game, sizeof(game)) < 0) {
					exit(EXIT_FAILURE);
				}
				
				if(read(clientSockFd, &m, sizeof(m)) < 0) {
					exit(EXIT_FAILURE);
				}
							
				print_dealer_informations(dealer);
				print_players_informations(game);
				
				total_value_player += ntohs(card.value);
				printf("%d\n", total_value_player);
				
				// If the player draws and exceeds 21 or exceeds the dealer score if he stopped to play.
				
				if(total_value_player > 21 || ((total_value_player > dealer.total_value_cards) && dealer.stop)) {
					printf("%d -- Attendez le résultat --\n", total_value_player);
					while(waiting) {
						waiting_for_result(game, dealer, m);
					}
					
				}
				
				else if (m.type != CONTINUE) {
					printf("Client fermé\n");
					running = false;
				}
				
				printf("%s\n", m.message);
			}
			
			else if(m.type == STAY) {
				while(waiting) {
					printf("%d -- Attendez le résultat --\n", total_value_player);
					waiting_for_result(game, dealer, m);
				}
			}
			
			else if(m.type == INFO) {
			
				if(read(clientSockFd, &dealer, sizeof(dealer)) < 0) {
					exit(EXIT_FAILURE);
				}
				
				if(read(clientSockFd, &game, sizeof(game)) < 0) {
					exit(EXIT_FAILURE);
				}
				
				print_dealer_informations(dealer);
				print_players_informations(game);
			}
				
			else {
				printf("%s\n", m.message);
			
			}
			
		}
		close(clientSockFd);
	} else {
	
		printf("%s", m.message);
		close(clientSockFd);
		
	}
	
	return 0;
}
