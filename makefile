CC = gcc
CFLAGS = -Wall -Wconversion

all : client server

client.o : clientsocket.h game.h client.c

client : clientsocket.o game.o 

server.o : serversocket.h game.h server.c

server : serversocket.o game.o

game.o : game.h game.c

clientsocket.o : clientsocket.h clientsocket.c

serversocket.o : serversocket.h clientsocket.c

clean:
	rm -f *.o
	rm -f client
	rm -f server
