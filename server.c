#include "serversocket.h"

void init_client_fd() {

	struct Status status = {false, false, false}; 
	struct Player player = {0, status, {}, 0, 0}; 
	
	for(int c = 0; c < MAXPLAYERS; c++) {
		game.players[c] = player;
	}
}

void init_dealer() {
	struct Dealer tmpDealer = {false, {}, 0, 0};
	dealer = tmpDealer;
}

int main() {
	
	rounds = 0;
	current_player = 0;
	running = true; 
	
	init_client_fd();
	init_dealer();
	
	init_deck();
	shuffle();
	
	if(init_socket()) {
		perror("Socket creation failed..");
		exit(EXIT_FAILURE);
	}

	init_addr();

	if (bind_socket()) {
		perror("Bind failed..");
		exit(EXIT_FAILURE);
	}

	if (listen_socket()) {
		perror("Listen failed..");
		exit(EXIT_FAILURE); 
	}
	
	FD_ZERO(&fds);
	FD_ZERO(&rfds);
	
	FD_SET(server_socket, &fds);
	
	fdmax = server_socket;
    	
	while(running) {
	
		rfds = fds;
		
    		if(select(fdmax + 1, &rfds, NULL, NULL, NULL) < 0) {
    			perror("Select error");
    			exit(EXIT_FAILURE);
    		}
		
		for(int fd = 0; fd <= fdmax; fd++) {
			if(FD_ISSET(fd, &rfds)) {					
				if(fd == server_socket) {
					if(current_client < MAXPLAYERS) {
						select_accept_client();	
					} 
					
					else if(current_client == MAXPLAYERS) {
						refuse_client();
					}
	
				} else {

					handle_command(fd);				
				}
			}
		}			
	}
	
	close(server_socket);
    	return 0;
}
