#ifndef DATAH
#define DATAH

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define MAXLINE 255
#define MAXCARDS 21
#define MAXPLAYERS 2

struct Card {
	char rank;
	char suit;
	uint16_t value;
};   

struct Message {
	char type;
	char message[MAXLINE];
};

struct Status {
	bool stop;
	bool stand;
	bool win;
};

struct Player {
	uint16_t client_fd;
	struct Status status;
	struct Card cards [MAXCARDS]; 
	uint16_t number_cards;
	uint16_t total_value_cards;
};

struct Dealer {
	bool stop;
	struct Card cards [MAXCARDS]; 
	uint16_t number_cards;
	uint16_t total_value_cards;
};

struct Game{
	struct Player players[MAXCARDS];
};

#endif
