#ifndef GAMEH
#define GAMEH

#include <time.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h> 

#include "data.h"

#define MAXLINE 255
#define PLAY 'p'
#define STAY 's'
#define LOOSE 'l'
#define WIN 'w'
#define CONTINUE 'c'
#define INFO 'i'
#define EXIT 'e'
#define NOT_VALID 'n'

struct Card deck[52];

int count_draw; 

void init_deck();

void shuffle();

bool send_card_to_player(int client_fd);

bool send_fd_to_player(int client_fd, uint16_t fd);

bool send_info_to_player(int client_fd, struct Game game);

bool send_dealer_info_to_player(int client_fd, struct Dealer dealer);

bool send_message(int client_fd, struct Message m);

#endif
