#include "clientsocket.h"

bool init_socket() {
	return (clientSockFd = socket(AF_INET, SOCK_STREAM, 0)) < 0;
}

bool connect_server() {
        return connect(clientSockFd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0;
}

void init_addr() {
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);
}

void print_players_informations(struct Game game) {

	printf("-----------------------------\n");
	for (int i=0; i < MAXPLAYERS; i++) {
		if(game.players[i].number_cards != 0) {
			printf("%d.", i);
			for(int j=0; j<game.players[i].number_cards; j++) {
				printf("Carte :\n");
				printf("Rank -> %c - Suit -> %c - Value -> %d\n", game.players[i].cards[j].rank, 
				game.players[i].cards[j].suit, game.players[i].cards[j].value);
			}
			printf("Total : %d\n", game.players[i].total_value_cards);	
			
			if(!game.players[i].status.win && game.players[i].status.stop) {
				printf("Perdu\n");
			}
			else if(game.players[i].status.win && game.players[i].status.stop) {
				printf("Gagné\n");
			} else {
				printf("Continue\n");
			}
			
			printf("-----------------------------\n");
		}
			
	}
}

void print_dealer_informations(struct Dealer dealer) {
		
	printf("-----------------------------\n");
	for(int i=0; i<dealer.number_cards; i++) {
		printf("Carte du croupier:\n");
		printf("Rank -> %c - Suit -> %c - Value -> %d\n", dealer.cards[i].rank, 
		dealer.cards[i].suit, dealer.cards[i].value);
	}
	printf("Total : %d\n", dealer.total_value_cards);
	printf("-----------------------------\n");
}

/* The player is currently waiting until the end of the game.
   Each round, the player receive the results of the round. */
void waiting_for_result (struct Game game, struct Dealer dealer, struct Message m) { 
	if(read(clientSockFd, &dealer, sizeof(dealer)) < 0) {
		perror("Read error");
		exit(EXIT_FAILURE);
	}
	
	if(read(clientSockFd, &game, sizeof(game)) < 0) {
		perror("Read error");
		exit(EXIT_FAILURE);
	}
	
	if(read(clientSockFd, &m, sizeof(m)) < 0) {
		perror("Read error");
		exit(EXIT_FAILURE);
	}
	
	print_dealer_informations(dealer);
	print_players_informations(game);
	
	if(m.type != CONTINUE) {
		printf("Client fermé\n");
		waiting = false;
		running = false;
	}

	printf("%s\n", m.message);	
}
