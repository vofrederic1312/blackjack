#ifndef CLIENTSOCKETH
#define CLIENTSOCKETH

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "game.h"
 
#define PORT 8000

// Variables 

bool running, waiting;
int total_value_player;

int clientSockFd;

struct sockaddr_in servaddr;

// Functions

bool init_socket();

bool connect_server();

void init_addr();

void print_players_informations(struct Game game);

void print_dealer_informations(struct Dealer dealer);

void waiting_for_result(struct Game game, struct Dealer dealer, struct Message m);

#endif
