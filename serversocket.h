#ifndef SERVERSOCKETH
#define SERVERSOCKETH

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "game.h"
 
#define PORT 8000

// Variables

int server_socket, length, connect_fd, current_client, fdmax, rounds, current_player, count_draw;

bool running;

fd_set fds, rfds;

struct sockaddr_in servaddr, cliaddr;

struct Game game;
struct Message m;
struct Dealer dealer;

// Functions

bool init_socket();

bool bind_socket();

bool listen_socket();

void init_addr(); 

bool accept_client();

void select_accept_client();

void refuse_client();

void start_game();

void handle_command(int fd);

void draw(int fd);

void stay(int fd);

void info(int fd);

void handle_wrong_command(int fd);

void handle_wrong_turn(int fd);

void change_round();

void handle_draw(int fd);

void handle_stay(int fd);

void handle_dealer_draw();

#endif

